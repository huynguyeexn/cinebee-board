import { IBase } from '.';

export interface CustomerType extends IBase {
	name: string;
}
