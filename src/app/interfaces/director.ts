import { IBase } from '.';

export interface Director extends IBase {
	fullname: string;
	avatar: string;
	slug: string;
}
