export interface IBase {
	id?: number | string;

	deleted_at?: number;
	created_at?: number;
	updated_at?: number;

	pivot?: any;
}
