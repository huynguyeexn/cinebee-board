import { IBase } from '.';

export interface AgeRating extends IBase {
	name: string;
	description: string;
}
