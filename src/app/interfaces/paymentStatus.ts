import { IBase } from '.';

export interface PaymentStatus extends IBase {
	name: string;
}