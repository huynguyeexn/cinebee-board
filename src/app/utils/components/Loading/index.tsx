import React from 'react';

interface Props {}

export const LOADING = (props: Props) => {
	return (
		<div className="tw-w-full tw-flex tw-justify-center tw-items-center">loading</div>
	);
};
